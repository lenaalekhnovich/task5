package test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import parsing.DOMParserBuilder;
import parsing.ParserRunner;
import parsing.SAXParserBuilder;
import parsing.StAXParserBuilder;
import validator.ValidatorXSD;

/**
 * Created by Босс on 31.01.2017.
 */
public class ParsingTest {

    private static ParserRunner parserRunner;
    private static String fileName = "src/resources/info.xml";
    private static String schemaName = "src/resources/schema.xsd";

    @BeforeClass
    public static void init() {
        parserRunner = new ParserRunner();
    }

    @Test
    public void validateDocumentTest() {
        boolean check = ValidatorXSD.validateDocument(fileName, schemaName);
        Assert.assertTrue(check);
    }

    @Test
    public void DOMParsingTest() {
        parserRunner.setParseBuilder(new DOMParserBuilder());
        parserRunner.createTouristVouchers(fileName);
        Assert.assertNotNull(parserRunner.getVouchers());
    }

    @Test
    public void SAXParsingTest() {
        parserRunner.setParseBuilder(new SAXParserBuilder());
        parserRunner.createTouristVouchers(fileName);
        Assert.assertNotNull(parserRunner.getVouchers());
    }

    @Test
    public void StAXParsingTest() {
        parserRunner.setParseBuilder(new StAXParserBuilder());
        parserRunner.createTouristVouchers(fileName);
        Assert.assertNotNull(parserRunner.getVouchers());
    }
}
