package parsing;

import object.TouristVoucher;

import java.util.List;

/**
 * Created by Босс on 02.02.2017.
 */
public class ParserRunner {

    private ParserBuilder parserBuilder;

    public void setParseBuilder(ParserBuilder parseBuilder) {
        this.parserBuilder = parseBuilder;
    }

    public List<TouristVoucher> getVouchers() {
        return parserBuilder.getVouchers();
    }

    public void createTouristVouchers(String fileName) {
        parserBuilder.parse(fileName);
    }


    public static void main(String args[]){
        ParserRunner d = new ParserRunner();
        d.setParseBuilder(new SAXParserBuilder());
        d.createTouristVouchers("src/resources/info.xml");
        System.out.println(d.getVouchers());
    }
}
