package parsing;

/**
 * Created by Босс on 10.02.2017.
 */
public enum VoucherEnum {

    TOURISTVOUCHERS("touristVouchers"),
    RESTVOUCHER("restVoucher"),
    WELLNESSVOUCHER("wellnessVoucher"),
    EXCURSIONVOUCHER("excursionVoucher"),
    COUNTRY("country"),
    AMOUNTDAYSNIGHTS("amountDaysNights"),
    TRANSPORT("transport"),
    COST("cost"),
    HOTEL("hotel"),
    NAMEHOTEL("nameHotel"),
    STARS("stars"),
    FOOD("food"),
    ROOM("room"),
    TV("tv"),
    AIRCONDITIONING("airConditioning"),
    EMAIL("email"),
    RESORT("resort"),
    NAMERESORT("nameResort"),
    PROCEDURES("procedures"),
    PROCEDURE("procedure"),
    EXCURSIONS("excursions"),
    EXCURSION("excursion");

    private String value;

    private VoucherEnum(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
