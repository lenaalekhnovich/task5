package parsing;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

/**
 * Created by Босс on 31.01.2017.
 */
public class SAXParserBuilder extends ParserBuilder {

    static Logger logger = Logger.getLogger(SAXParserBuilder.class);

    private VoucherHandler handler;
    private XMLReader reader;

    public SAXParserBuilder() {
        handler = new VoucherHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(handler);
        } catch (SAXException e) {
            logger.error("negative argument: " + e);
        }
    }

    @Override
    public void parse(String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException e) {
            logger.error("negative argument: " + e);
        } catch (IOException e) {
            logger.error("negative argument: " + e);
        }
        vouchers = handler.getVouchers();
    }
}
